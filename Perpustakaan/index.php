<?php
	error_reporting(0);
	$connect = mysqli_connect('localhost','root','','perpustakaan');
	 // $query = "SELECT * FROM buku limit 10";
	 // $result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Perpustakaan</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  		<script src="https://cdn.rawgit.com/Dogfalo/materialize/fc44c862/dist/js/materialize.min.js"></script>
  		<script type="text/javascript" src="https://cdn.rawgit.com/pinzon1992/materialize_table_pagination/f9a8478f/js/pagination.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.1.min.js"></script>
	    <script src="jquery.tabledit.min.js"></script>
	    <script type="text/javascript" src="materialize.min.js"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="materialize.min.css">
	</head>
	<body>
		<div class="container">		
			<?php
				$query = $_POST['query'];
				if($query != ''){
					$result = mysqli_query($connect, "SELECT * FROM buku WHERE kd_buku LIKE '%".$query."%' OR
					judul_buku LIKE '%".$query."%'" );
				}else{ 
					$result = mysqli_query($connect, "SELECT * FROM buku limit 10");	
				}
			?>
<!-- 	<nav>
    <div class="nav-wrapper">
      <form action="" method="POST">
        <div class="input-field">
          <input id="search" type="search" required name="query">
          <label class="label-icon" for="search"><i class="material-icons">search</i></label>
          <i class="material-icons">close</i>
        </div>
      </form>
    </div>
  </nav> --> 
		   		<h3 align="center"><a href="index.php">Perpustakaan</a></h3><br/>  
		   		<nav>
   					<div class="nav-wrapper">
		   			    <form action="" method="POST">
					        <div class="input-field">
					        	<input id="search" type="search" required name="query">
					        	<label class="label-icon" for="search"><i class="material-icons">search</i></label>
					        	<i class="material-icons">close</i>
					        </div>
					    </form>
					</div>
				</nav>
		   	<div class="table-responsive"> 
		   		<table id="editable_table" class="table table-bordered table-striped">
					<br>
					<button onclick="window.location.href = 'input.php';" class="waves-effect waves-light btn">Tambah Data</button>
					<br>
		     		<thead>
		     			<br>
			   			<tr>
					        <th>Kode</th>
				    	    <th>Judul</th>
				    	    <th>No.Lemari</th>
				    	    <th>No.Rak</th>
				    	    <th>Nama Lemari</th>
				    	    <th>Jumlah Buku</th>
			    		</tr>
		   	 		</thead>
		   			<tbody>
		   				<?php
						    while($row = mysqli_fetch_array($result)){
						   		echo '
							    <tr>
								    <td>'.$row["kd_buku"].'</td>
								    <td>'.$row["judul_buku"].'</td>
								    <td>'.$row["no_lemari"].'</td>
								    <td>'.$row["no_rak"].'</td>
								    <td>'.$row["nama_lemari"].'</td>
								    <td>'.$row["jumlah_buku"].'</td>
							    </tr>
						    	';
						    }
					    ?>
			    	</tbody>
		  		</table>
		  		<div class="col-md-12 center text-center">
			    	<span class="left" id="total_reg"></span>
		            <ul class="pagination pager" id="myPager"></ul>
        		</div>
		  	</div>  
		</div>  
		<?php
			$rowSQL = mysqli_query($connect, "SELECT MAX(kd_buku) as max FROM buku");
			$row = mysqli_fetch_array( $rowSQL );
			$largestNumber = $row['max']+1;
		?>
	</body>
</html>
<script>
	$(document).ready(function(){     
		$('#editable_table').Tabledit({
		    url:'action.php',
		    columns:{
		    	identifier:[0, "kd_buku"],
		    	editable:[[1, 'judul_buku'], [2, 'no_lemari'], [3, 'no_rak'], [4, 'nama_lemari'], [5, 'jumlah_buku']]
		    },
	    	restoreButton:false,
		    onSuccess:function(data, textStatus, jqXHR){
		        if(data.action == 'delete'){
		        	$('#'+data.id).remove();
		        }
		    }
	    });

	 // 	$("#addRow").click(function() {
		// 	var tableditTableName = '#editable_table';  // Identifier of table
		// 	var newID = parseInt($(tableditTableName + " tr:last").attr("id")) + 1;
		// 	// var	newID = <?php 
		// 	// echo $largestNumber; 
		// 	// ?> 
		// 	var clone = $("table tr:last").clone(); 
		// 	$(".tabledit-span", clone).text(""); 
		// 	$(".tabledit-input", clone).val(""); 
		// 	clone.prependTo("table"); 
		// 	$(tableditTableName + " tbody tr:first").attr("id", newID); 
		// 	$(tableditTableName + " tbody tr:first td .tabledit-span.tabledit-identifier").text(newID); 
		// 	$(tableditTableName + " tbody tr:first td .tabledit-input.tabledit-identifier").val(newID); 
		// 	$(tableditTableName + " tbody tr:first td:last .tabledit-edit-button").trigger("click");
		// });

		// $("#add").click(function(e){
		//     var table = $(this).attr('for-table');  //get the target table selector
		//     var $tr = $(table + ">tbody>tr:last-child").clone(true, true);  //clone the last row
		//     var nextID = parseInt($tr.find("input.tabledit-identifier").val()) + 1; //get the ID and add one.
		//     $tr.find("input.tabledit-identifier").val(nextID);  //set the row identifier
		//     $tr.find("span.tabledit-identifier").text(nextID);  //set the row identifier
		//     $(table + ">tbody").append($tr);    //add the row to the table
		//     $tr.find(".tabledit-edit-button").click();  //pretend to click the edit button
		//     $tr.find("input:not([type=hidden]), select").val("");   //wipe out the inputs.
		// });

		$('#editable_table').pageMe({
		    pagerSelector:'#myPager',
		    activeColor: 'blue',
		    prevText:'Anterior',
		    nextText:'Siguiente',
		    showPrevNext:true,
		    hidePageNumbers:false,
		    perPage:10
		});
	});  
</script>