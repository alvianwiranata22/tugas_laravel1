
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

CREATE TABLE `tb_kesimpulan` (
  `kode_kesimpulan` int(11) NOT NULL,
  `solusi` varchar(200) NOT NULL,
  `fakta` varchar(100) NOT NULL,
  `oleh` varchar(100) NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



INSERT INTO `tb_kesimpulan` (`kode_kesimpulan`, `solusi`, `fakta`, `oleh`, `status`) VALUES
(1, 'Sabar', 'Sabar menerima perlakukan mahasiswa dan sejawatnya', 'pakar', 'setuju'),
(2, 'Sabar', 'Sabar menghadapi masalah', 'pakar', 'setuju'),
(3, 'Sabar', 'Sabar menunggu jawaban dari Tuhan', 'pakar', 'setuju'),
(4, 'Empati', 'Menolong mahasiswa yang sedang bermasalah kuliah', 'pakar', 'setuju'),
(5, 'Empati', 'Menolong sejawat yang bermasalah dalam bekerja', 'pakar', 'setuju'),
(6, 'Empati', 'Menolong pimpinan yang butuh pertolongan', 'pakar', 'setuju'),
(7, 'Rohani', 'Rajin beribadah', 'pakar', 'setuju'),
(8, 'Rohani', 'Menghubungkan hal-hal rohani dalam perkulihan', 'pakar', 'setuju'),
(9, 'Rohani', 'Mengajak mahasiswa untuk beriman', 'pakar', 'setuju'),
(10, 'Tidak ada hasil 1', 'Tidak sabar menerima perlakuan mahasiswa dan sejawatnya', 'pakar', 'setuju'),
(11, 'Tidak ada hasil 1', 'Tidak menolong mahasiswa yang sedang bermasalah kuliah', 'pakar', 'setuju'),
(12, 'Tidak ada hasil 1', 'Tidak rajin beribadah', 'pakar', 'setuju'),
(13, 'Tidak ada hasil 2', 'Tidak sabar menerima perlakuan mahasiswa dan sejawatnya', 'pakar', 'setuju'),
(14, 'Tidak ada hasil 2', 'Tidak menolong mahasiswa yang sedang bermasalah kuliah', 'pakar', 'setuju'),
(15, 'Tidak ada hasil 2', 'Rajin beribadah', 'pakar', 'setuju'),
(16, 'Tidak ada hasil 2', 'Tidak menghubungkan hal-hal rohani dalam perkuliahan', 'pakar', 'setuju'),
(17, 'Tidak ada hasil 3', 'Tidak sabar menerima perlakuan mahasiswa dan sejawatnya', 'pakar', 'setuju'),
(18, 'Tidak ada hasil 3', 'Tidak menolong mahasiswa yang sedang bermasalah kuliah', 'pakar', 'setuju'),
(19, 'Tidak ada hasil 3', 'Rajin beribadah', 'pakar', 'setuju'),
(20, 'Tidak ada hasil 3', 'Menghubungkan hal-hal rohani dalam perkuliahan', 'pakar', 'setuju'),
(21, 'Tidak ada hasil 3', 'Tidak mengajak mahasiswa untuk beriman', 'pakar', 'setuju')
;



CREATE TABLE `tb_pertanyaan` (
  `kode_pertanyaan` varchar(50) NOT NULL,
  `isi_pertanyaan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




INSERT INTO `tb_pertanyaan` (`kode_pertanyaan`, `isi_pertanyaan`) VALUES
('p1', 'Apakah anda sabar menerima perlakukan dari mahasiswa ?'),
('p2', 'Apakah anda sabar dalam menghadapi masalah ?'),
('p3', 'Apakah anda sabar menunggu jawaban dari Tuhan ?'),
('p4', 'Apakah anda menolong mahasiswa yang sedang bermasalah kuliah ?'),
('p5', 'Apakah anda dapat menolong sejawat yang sedang dalam masalah ?'),
('p6', 'Apakah anda menolong pimpinan yang butuh pertolongan ?'),
('p7', 'Apakah anda rajin beribadah ?'),
('p8', 'Apakah anda menghubungkan hal-hal rohani dalam perkuliahan ?'),
('p9', 'Apakah anda mengajak mahasiswa untuk beriman ?')
;




CREATE TABLE `tb_solusi` (
  `kode_solusi` varchar(50) NOT NULL,
  `isi_solusi` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




INSERT INTO `tb_solusi` (`kode_solusi`, `isi_solusi`) VALUES
('s1', 'Sabar'),
('s2', 'Empati'),
('s3', 'Rohani'),
('s4', 'Tidak ada hasil 1'),
('s5', 'Tidak ada hasil 2'),
('s6', 'Tidak ada hasil 3')
;




ALTER TABLE `tb_kesimpulan`
  ADD PRIMARY KEY (`kode_kesimpulan`);



ALTER TABLE `tb_pertanyaan`
  ADD PRIMARY KEY (`kode_pertanyaan`);



ALTER TABLE `tb_solusi`
  ADD PRIMARY KEY (`kode_solusi`);



ALTER TABLE `tb_kesimpulan`
  MODIFY `kode_kesimpulan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=150;
COMMIT;
