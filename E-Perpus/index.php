<?php
	error_reporting(0);
	$connect = mysqli_connect('localhost','root','','perpustakaan');
	 // $query = "SELECT * FROM buku limit 10";
	 // $result = mysqli_query($connect, $query);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Perpustakaan</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />  
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>            
	    <script src="jquery.tabledit.min.js"></script>
	</head>
	<body>
		<div class="container">		
			<?php
				$query = $_POST['query'];
				if($query != ''){
					$result = mysqli_query($connect, "SELECT * FROM buku WHERE kd_buku LIKE '%".$query."%' OR
					judul_buku LIKE '%".$query."%'" );
				}else{ 
					$result = mysqli_query($connect, "SELECT * FROM buku limit 10");	
				}
			?>
		   	<div class="table-responsive">  
		   		<h3 align="center">Perpustakaan</h3><br />  
		   		<table id="editable_table" class="table table-bordered table-striped">
		   			<form action="" method="POST">
						<input type="text" name="query" placeholder="Cari Buku" autofocus size="30" />
						<input type="submit" name="cari" value="Cari" autocomplete ="off" />
					</form>
					<br>
		     		<thead>
			   			<tr>
					        <th>Kode</th>
				    	    <th>Judul</th>
				    	    <th>No.Lemari</th>
				    	    <th>No.Rak</th>
				    	    <th>Nama Lemari</th>
				    	    <th>Jumlah Buku</th>
			    		</tr>
		   	 		</thead>
		   			<tbody>
		   				<?php
						    while($row = mysqli_fetch_array($result)){
						   		echo '
							    <tr>
								    <td>'.$row["kd_buku"].'</td>
								    <td>'.$row["judul_buku"].'</td>
								    <td>'.$row["no_lemari"].'</td>
								    <td>'.$row["no_rak"].'</td>
								    <td>'.$row["nama_lemari"].'</td>
								    <td>'.$row["jumlah_buku"].'</td>
							    </tr>
						    	';
						    }
					    ?>
			    	</tbody>
		  		</table>
		  	</div>  
		</div>  
		<?php
			$rowSQL = mysqli_query($connect, "SELECT MAX(kd_buku) as max FROM buku");
			$row = mysqli_fetch_array( $rowSQL );
			$largestNumber = $row['max']+1;
		?>
	</body>
</html>
